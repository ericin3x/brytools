#!/bin/python3
#The goal behind this class is to try and encapsulate the Ajax query response.
from browser import ajax
import urllib.parse
import sys
import json
import traceback
import cpts
pts = cpts.cpts('fajax')


    
class sajax:
    def __init__(self):
        self.timeout = 20
        self.data = None
    
    def request(self, Ajax_URL, Ajax_mode ="GET", arg_dict=None):
                
        # initial state of URL and data
        working_URL = Ajax_URL
        working_arg = arg_dict

        # set up ajax call.
        req = ajax.ajax()
        req.bind('complete',self.request_complete)
        req.set_timeout(self.timeout,self.err_msg)

        # put args into proper form
        if 'GET' == Ajax_mode:
           # convert to get url encoded pairs
           url_arg = ""
           if arg_dict:
               url_arg = '?' + urllib.parse.urlencode(arg_dict)
           working_URL = Ajax_URL + url_arg
           working_arg = {}
           
        req.open(Ajax_mode, working_URL, True)
        req.send(working_arg)

    def request_complete(self,req):
        """         """
        pts.tick('in req compl: %s' % req.status)
        if req.status == 200 or req.status == 0:
            try:
                pts.tick("in R_C pre jason: %s"% req.text)
                self.data = json.loads(req.text)
                pts.tick("in R_C post jason: %s"% str(self.data))
                tb="what error"

                self.app_process()
            except:
                tb = traceback.format_exc()
            else:
                tb = "No error"
            finally:
                print(tb)
                #pass
        else:
            print ("should not get here")
    
            
    def app_process(self, ):
        """do what you need to do with this ajax query"""
        pass
                        
    def err_msg(self,):

        document["result"].html = "server didn't reply after %s seconds" %self.timeout
        

