import datetime
class cpts:
    def __init__(self, prefix):
        self.last_pts = None
        self.prefix = prefix
        
    def tick(self,tag):
        now = datetime.datetime.now()
        if self.last_pts:
            delta_pts = now-self.last_pts
        else:
            delta_pts = now-now

        self.last_pts = now

        str_delta_pts = str(delta_pts)

        print( '{} -- {} Timestamp: {:%Y-%m-%d %H:%M:%S} {}'.format(self.prefix, tag, now, str_delta_pts))

